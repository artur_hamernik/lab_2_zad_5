const _ = require('lodash');

const collections = [
 {},,
 15,
 "hello@test.pl",
 null,
 ['aaa', 'bbb', 5],
 'admin@gmail.com',
 undefined,
 'a34@\ahoo.com',
 '321@a',
 '321.pl'
];
const reg = /^[-\w\.]+@([-\w]+\.)+[a-z]+$/i;

const valid = ['tomek@drogimex.pl',
               'tomek.sochacki@drogimex.pl',
               'tomek@urzad.gov.pl',
               'ToMeK@drogimex.pl',
               'tomek123@domena111.pl',
               'tomek_123@domena.pl',
               '11tomek@domena.pl'];

function getMails(collections){
  console.log(_.sortBy(_.filter(collections, function(o){ return reg.test(o)})));
}

getMails(collections);
